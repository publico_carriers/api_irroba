# Introdução 
API de Integracao Carriers e Irroba

# Requisição
> Homologacao: http://35.247.213.54/api/client/

> Producao: https://www.carriers.com.br/api/client/

```JSON
 curl -X GET \
   http://35.247.213.54/api/client/  \
  -H 'Authorization: Bearer API-KEY' \
  -H 'cache-control: no-cache'
  ```

* Cadastro de pedidos:
    * [Cadastro de pedidos](ConsultaPedidos/CadastroPedidos.md)

* Consulta Tracking:
    * [Consulta de Tracking](ConsultaPedidos/ConsultaTracking.md)

* Consulta Situação pedido:
    * [Consulta Situação pedido](ConsultaPedidos/ConsultaSituacaoPedido.md)

* Consulta Frete:
    * [Consulta de Frete](ConsultaPedidos/ConsultaFrete.md)
