# Introdução
A API para consultar Tracking de pedidos


### Solicitação GET
> /Irroba/Tracking/{trackingCode}

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
trackingCode|Chave identificadora do pedido|sim|String
 


### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "error": {
        "code": "500",
        "message": "rro no servidor"
    }
}
```

Erros de requisição (erro 400)
```JS
{
    "error": {
        "code": "400",
        "message": "dados de Tracking não encontrado"
    }
}
```



## Resposta

```JS
{
    "trackingCode": "100050467",
    "carrierName": "Carriers",
    "idItemParceiro": 9194476,
    "events": [
        {
            "date": "25-06-2019 09:47:59",
            "name": "ENTREGA REALIZADA",
            "description": "Entregue"
        },
        {
            "date": "25-06-2019 07:36:52",
            "name": "EM ROTA DE ENTREGA",
            "description": "Em rota de entrega"
        },
        {
            "date": "25-06-2019 00:17:36",
            "name": "SEPARADO",
            "description": "Em preparação para transporte"
        },
        {
            "date": "23-06-2019 23:32:36",
            "name": "SEPARADO",
            "description": "Em preparação para transporte"
        },
        {
            "date": "21-06-2019 00:42:05",
            "name": "SEPARADO",
            "description": "Em preparação para transporte"
        },
        {
            "date": "19-06-2019 20:00:47",
            "name": "RECEBIDO NA BASE",
            "description": "Na filial distribuidora"
        },
        {
            "date": "19-06-2019 10:28:53",
            "name": "COLETADO",
            "description": "Recebido no CD da transportadora"
        }
    ]
}
```
