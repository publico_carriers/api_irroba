# Introdução
A API para consultar Situação do pedido


### Solicitação GET
> /Irroba/orderSituation/{trackingCode}

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
trackingCode|Chave identificadora do pedido|sim|String
 


### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "error": {
        "code": "500",
        "message": "Erro no servidor"
    }
}
```

Erros de requisição (erro 400)
```JS
{
    "error": {
        "code": "400",
        "message": "dados de Situação não encontrado"
    }
}
```
# Lista de Status
|idStatus|Status|Descrição             
|----------------|---------------|---------------|
22|	COLETADO|	Recebido no CD da transportadora
24|	CANCELADO|	Pedido não recebido pela transportadora
31|	SEPARADO|	Em preparação para transporte
50|	EM TRANSFERÊNCIA PARA A BASE|	Em transferência para filial Distribuidora
51|	RECEBIDO NA BASE|	Na filial distribuidora
100|	EM ROTA DE ENTREGA|	Em rota de entrega
101|	ENTREGA REALIZADA|	Entregue
102|	INSUCESSO DE ENTREGA|	Com insucesso de entrega
150|	EM DROPPOINT|	Em Drop Point
151|	EDROP REALIZADO|	Entregue
200|	PENDENCIA DE ENTREGA|	Na filial distribuidora
411|	DEVOLUÇÃO REALIZADA|	Pedido devolvido à loja


## Resposta

```JS
{
    "trackingCode": "100050798",
    "carrierName": "Carriers",
    "idItemParceiro": 9237888,
    "events": {
        "date": "25-06-2019 03:13:25",
        "idStatus": 31,
        "name": "SEPARADO",
        "description": "Em preparação para transporte"
    }
}
```
