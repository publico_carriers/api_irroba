# Introdução
A API para consultar frete


### Solicitação POST
> /Irroba/Rates/{postalcode}

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
declaredValue|Valor do produto |sim|Numeric
weight|Peso do produto|sim|Numeric
height|Altura da embalagem|sim|Numeric
width|Largura da embalagem|sim|Numeric
length|comprimento da embalagem|sim|Numeric


### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "error": {
        "code": "500",
        "message": "Erro no servidor"
    }
}
```

Erros de Cep
```JS
{
    "error": {
        "code": "400",
        "message": "Cep Não encontrado"
    }
}
```

```JS
{
    "error": {
        "code": "400",
        "message": "Cep Não atendido"
    }
}
```


Erros de requisição (erro 400)

```JS
{
    "error": {
        "code": "400",
        "message": "campo {Nome do campo} invalido"
    }
}
```
```JSON
`curl -X POST \
  {host}/Irroba/Rates/06385843\
  -H 'Authorization: Bearer API-KEY' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{"declaredValue": 20.00, "weight": 2, "height": 100, "width": 10, "length": 10}'
  ```


## Resposta

```JS
{
    "postalCode": "06385843",
    "shippingServices": [
        {
            "name": "NORMAL",
            "quantityDays": 3,
            "price": 24.8
        }
    ]
}
```
