# Introdução
A API para cadastrar pedidos


### Solicitação POST
> /Irroba/newOrder

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
volumes|Dados dos volumes|sim|Objeto **Volumes**
skus|Informações de referência|sim|Objeto **Skus**
invoice|Informações da Nota fiscal|não|Objeto **Invoice**
trackingId|Chave identificadora do pedido|sim|String
observation|Observações gerais|sim|String
recipient|Nome e endereço do destinatário|sim|Objeto **Recipient**
totalValue|Valor total da nota fiscal|sim|Numeric
CNPJ|cnpj do cliente|nao|Numeric

### Objeto **Volumes**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
declaredValue|Valor declarado do volume|não|Numeric
dimensions|Dimensões do volume|não|Objeto **Dimensions**

### Objeto **Dimensions**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
height|Altura(cm)|sim|Numeric
width|Largura(cm)|sim|Numeric
length|Comprimento(cm)|sim|Numeric
weight|Peso bruto(cm)|sim|Numeric

### Objeto **Skus**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
description|Descrição do item|não|Numeric

### Objeto **Invoice**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
id|Número da nota fiscal|sim|Numeric
key|Chave de acesso da nota fiscal (44 caracteres)|sim|Numeric
type|Valor padrão = NFe|sim|Numeric

### Objeto **Recipient**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
fullName|Nome completo|sim|String
IE|Inscrição estadual|nao|Numeric
email| E-mail|não|String
phone|Telefone (até 14 caracteres)|não|Numeric
document|CPF ou CNPJ|sim|Numeric
address|Endereço|sim|**Address**

### Objeto **Address**
|Campo|Descricao|Obrigatório|Tipo
|----------------|---------------|---------------|---------------|
street|Logradouro|sim|String
number|Número|sim|String
addressLine2| Complemento|não|String
reference|Referência|sim|Numeric
neighborhood|Bairro|sim|Numeric
city|Cidade|sim|String
state|Estado|sim|String
postalCode|CEP|sim|Numeric

```JSON
curl -X POST \
  {host}/Irroba/newOrder \
  -H 'Accept: application/xml' \
  -H 'Authorization: Bearer API_KEY' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
   "items":[
      {
         "volumes":[
            {
               "declaredValue":1.00,
               "dimensions":{
                  "height":"10",
                  "width":"20",
                  "length":"30",
                  "weight":"40"
               }
            },
            {
               "declaredValue":1.00,
               "dimensions":{
                  "height":"50",
                  "width":"60",
                  "length":"70",
                  "weight":"80"
               }
            }
         ],
         "skus":[
            {
               "description":"Bermuda SKU Variação 1 Azul"
            }
         ],
         "invoice":{
            "id":"12345678-9",
            "key":"12345678901234567890123456789012345678901234",
            "type":"NFe"
         },
         "trackingId":"vvvvv",
         "partnerItemId":"252512-00001",
         "observation":"Item frágil",
         "recipient":{
            "fullName":"João Destinatário",
            "phone":"(11) 91111-1111",
            "email":"exemplo-contato@mandae.com.br",
            "document":"03356435310",
             "IE":"03356435310",
            "address":{
               "postalCode":"06385843",
               "street":"Rua Martinopolis",
               "number":"132",
               "neighborhood":"jd Ampermarg",
               "addressLine2":"Apto 255", 
               "city":"carapicuiba",
               "state":"SP",
               "country":"BR",
               "reference":"Próximo a estação CPTM Vila Leopoldina"
            }
         },
         "totalValue":42.0
      }
   ],

   "observation":null,
   "CNPJ":"28091518000105"
}'
```

### Códigos de erro 
Erros de requisição (erro 400)
```JS
{
    "error": {
        "code": "400",
        "message": "Erro no sistema"
    }
}
```

## Resposta
Cadastro sucesso
```JS
{
    "id": 551,
    "CNPJ":28091518000105,
    "items": [
        {
            "trackingId": "vvvvv",
            "partnerItemId": "3283",
            "reference": "nada",
            "recipient": {
                "fullName": "JOAO DESTINATARIO",
                "phone": "(11) 91111-1111",
                "email": "EXEMPLO-CONTATO@MANDAE.COM.BR",
                "address": {
                    "postalCode": "06385-843",
                    "street": "RUA MARTINOPOLIS 132",
                    "number": "132",
                    "neighborhood": "JD AMPERMARG",
                    "city": "CARAPICUIBA",
                    "state": "SP",
                    "country": "BR",
                    "reference": "PROXIMO A ESTACAO CPTM VILA LEOPOLDINA"
                }
            },
            "volumes": [
                {
                    "declaredValue": 1,
                    "dimensions": {
                        "height": "0.00",
                        "width": "0.00",
                        "length": "0.00",
                        "weight": 1,
                        "codigoDeBarras": "C@451234567812S"
                    }
                }
            ],
            "shippingService": "Normal",
            "invoice": {
                "id": "12345678",
                "key": "12345678901234567890123456789012345678901234",
                "type": "NFe"
            },
            "platform": "api",
            "inputChannel": "api"
        }
    ]
}
```
Pedidos já cadastrados ou com erro de requisição 

```JS
{
    "id": 0,
    "items": []
}
```